$.fn.htable=function(table,url,options={}){
    var settings=$.extend({
        delay:500,
        timeoutID:0,
        columns:[]
    },options);
    if($('.pagination').length<=0){
        $(table).after($('<ul></ul>').addClass("pagination").attr('role','navigation'));
    }
    fetchData(null,url,{});
    function fetchData(c,ref,param){
        $.get(ref,param).done(
            function(response){
                $(table+'>tbody').html('');
                $.each(response.data,function(i,data){
                    row=$('<tr></tr>');
                    $.each(settings.columns,function(i,column){
                        row.append($('<td></td>').html(column.hasOwnProperty('render')?column.render(data):data[column.name]));
                    });
                    $('table.search:first').append(row);
                });

                $('ul.pagination').html('');
                $('ul.pagination').append(
                    $('<li></li>').addClass("page-item").append(
                        $('<a></a>').addClass('page-link').attr('href',response.prev_page_url).attr('aria-label',"« Previous").attr('rel',"prev").html("«")
                    ),
                    $('<li></li>').addClass("page-item disabled").attr('aria-disabled','true').append(
                        $('<span></span>').addClass('page-link').html(response.current_page+' of '+response.last_page)
                    ),
                    $('<li></li>').addClass("page-item").append(
                        $('<a></a>').addClass('page-link').attr('href',response.next_page_url).attr('aria-label',"Next »").attr('rel',"next").html("»")
                    )
                );

            }
        );
    }
    $(this).on('keyup',function(){
        c=$(this);
        clearTimeout(settings.timeoutID);
        settings.timeoutID=setTimeout(fetchData,settings.delay,c,url,{q:c.val()});
    });
    $('.pagination').on('click','a',function(e){
        e.stopPropagation();
        e.preventDefault();
        if($(this).attr('href')){
            fetchData(null,url+$(this).attr('href'),{});
        }
        return false;
    });
}