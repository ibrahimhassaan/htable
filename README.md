# hTable

js library to render table from eloquent response. similar to datatables

usage:

$('#search_button').htable('#table',
"base url",
{
columns:[
    {name:'id'},
    {name:'title'},
    {name:'student_id'},
    {name:'status'},
    {render:function(data){ //custom return
        return $('<div></div>').append(
            $('<a></a>').addClass('btn btn-success mx-1').attr('href',"/post/"+data.id).html('show'),
            $('<a></a>').addClass('btn btn-primary mx-1').attr('href',"/post/"+data.id+"/edit").html('edit'),
            $('<form></form>').attr('action',"/post/"+data.id).attr('method','POST').addClass("d-inline").append(
                $('<input>').attr('name','_method').attr('type','hidden').val('Delete'),
                $('<input>').attr('name','_token').attr('type','hidden').val($('meta[name="csrf-token"]').attr('content')),
                $('<input>').addClass("btn btn-danger").attr('type','submit').val('Delete')
            )
        );
    }}
]
});